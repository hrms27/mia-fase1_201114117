#include <stdio.h>
#include <stdlib.h>
#include "Disk.h"
#include "Partitions.h"
#include "Reports.h"

int initnodestate = 0;

int parametersflags[] = {0,0,0,0,0,0,0,0,0};

int commandtype = 0;

int firstparameter = 1;

IDS ids;

int sizedisk;
char pathstring[100] = "";
char unitchar;
char typechar;
char fitstring[2] = {'w','f'};
char namestring[40] = "";
char idstring[40] = "";
int adddisk;
int deleteform;

int main(void)
{
    int exit = 0;
    printf("Escriba sus comandos \n");
    char leters[20] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t'};
    int i =0;

    for(i=0;i<20;i++){
        ids.lets[i].let= leters[i];
        int j=0;
        for(j=0;j<50;j++){
            ids.lets[i].nums[j].num = j;
        }
    }

    do{
        char commandconsole[200];
        fgets(commandconsole,sizeof(commandconsole),stdin);
        parserCommand(commandconsole);
    }while(exit == 0);

    return 0;
}



void parserCommand(char *command){

    int i = 0;

    int commandlen = strlen(command);

    int nodestate = initnodestate;

    int commentline = 0;

    int pathcounter = 0;
    int namecounter = 0;
    int idcounter = 0;
    int negative = 1;

    for(i ;i < commandlen;i++){

        char commandchar = tolower(command[i]);

        switch(nodestate){
        case 0:
            initializeparameters();
            if(commandchar == 'm'){
                nodestate = 1; // mkdisk mount
            }else if(commandchar == 'r'){
                nodestate = 29; // rmdisk rep
            }else if(commandchar == 'f'){
                nodestate = 2; // fdisk
                commandtype = 5;
            }else if(commandchar == 'u'){
                nodestate = 67; // unmount
                commandtype = 6;
            }else if(commandchar == 'e'){
                nodestate = 74; // exec
                commandtype = 7;
            }else if(commandchar == '#'){
                nodestate = 20;
                commentline = 1;
            }else{
                printf("Error no posee un comando inicial valido \n");
                return 0;
            }
            break;
        case 1:
            if(commandchar == 'k'){
                nodestate = 2;
                commandtype = 1;
            }else if(commandchar == 'o'){
                nodestate = 63;
                commandtype = 2;
            }else{
                printf("Error se esperaba una k o una o \n");
                return 0;
            }
            break;
        case 2:
            if(commandchar == 'd'){
                nodestate = 3;
            }else{
                printf("Error  se esperaba d \n");
                return 0;
            }
            break;
        case 3:
            if(commandchar == 'i'){
                nodestate = 4;
            }else{
                printf("Error se esperaba i 3  \n");
                return 0;
            }
            break;
        case 4:
            if(commandchar == 's'){
                nodestate = 5;
            }else{
                printf("Error se esperaba s  \n");
                return 0;
            }
            break;
        case 5:
            if(commandchar == 'k'){
                nodestate = 6;
            }else{
                printf("Error se esperaba k \n");
                return 0;
            }
            break;
        case 6:
            if(commandchar == ' '){
                nodestate = 7;
            }else{
                printf("Error se esperaba un espacio  \n");
                return 0;
            }
            break;
        case 7:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '-'){
                nodestate = 8;
            }else if(commandchar == '\n'){
            }else if(commandchar == '\\'){
                initnodestate = 7;
                return 0;
            }else{
                printf("Error se esperaba caracteres de espacio, negatividad, salto de linea y slash \n");
                return 0;
            }
            break;
        case 8:
            if(commandchar == 's'){
                nodestate = 9;
                parametersflags[0] = 1 + firstparameter;
                firstparameter = 0;
            }else if(commandchar == 'p'){
                nodestate = 15;
                parametersflags[1] = 1;
            }else if(commandchar == 'u'){
                nodestate = 23;
                parametersflags[2] = 1;
            }else if(commandchar == 't'){
                nodestate = 30;
                parametersflags[3] = 1;
            }else if(commandchar == 'f'){
                nodestate = 36;
                parametersflags[4] = 1;
            }else if(commandchar == 'd'){
                nodestate = 42;
                parametersflags[5] = 1 + firstparameter;
                firstparameter = 0;
            }else if(commandchar == 'n'){
                nodestate = 55;
                parametersflags[6] = 1;
            }else if(commandchar == 'i'){
                nodestate = 70;
                parametersflags[7] = 1;
            }else if(commandchar == 'a'){
                nodestate = 79;
                parametersflags[8] = 1 + firstparameter;
                firstparameter = 0;
            }else{
                printf("Error no es inicio de parametro \n");
                return 0;
            }
            break;
        case 9:
            if(commandchar == 'i'){
                nodestate = 10;
            }else{
                printf("Error se esperaba i  9 \n");
                return 0;
            }
            break;
        case 10:
            if(commandchar == 'z'){
                nodestate = 11;
            }else{
                printf("Error se esperaba z \n");
                return 0;
            }
            break;
        case 11:
            if(commandchar == 'e'){
                nodestate = 12;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 12:
            if(commandchar == '='){
                nodestate = 13;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 13:
            if(isdigit(commandchar)){
                nodestate = 14;
                sizedisk = (int)commandchar - 48;
            }else{
                printf("Error se esperaba un digito \n");
                return 0;
            }
            break;
        case 14:
            if(isdigit(commandchar)){
                nodestate = 14;
                sizedisk = (sizedisk * 10) + ((int)commandchar - 48);
            }else if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba digito, espacio o salto de linea \n");
                return 0;
            }
            break;
        case 15:
            if(commandchar =='a'){
                nodestate = 16;
            }else{
                printf("Error se esperaba a \n");
                return 0;
            }
            break;
        case 16:
            if(commandchar=='t'){
                nodestate = 17;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 17:
            if(commandchar=='h'){
                nodestate = 18;
            }else{
                printf("Error se esperaba h \n");
                return 0;
            }
            break;
        case 18:
            if(commandchar == '='){
                nodestate = 19;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 19:
            if(commandchar == '\"'){
                nodestate = 21;
            }else{
                nodestate = 20;
                pathstring[pathcounter] = command[i];
                pathcounter += 1;
            }
            break;
        case 20:
            if(commandchar == ' '){
                if(commentline == 1){
                    nodestate = 20;
                }else{
                    nodestate = 7;
                }
            }else if(commandchar == '\n'){
            }else{
                pathstring[pathcounter] = command[i];
                pathcounter += 1;
            }
            break;
        case 21:
            if(commandchar == '\"'){
                nodestate = 22;
            }else{
                pathstring[pathcounter] = command[i];
                pathcounter += 1;
            }
            break;
        case 22:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 23:
            if(commandchar == 'n'){
                nodestate = 24;
            }else{
                printf("Error se esperaba n 23\n");
                return 0;
            }
            break;
        case 24:
            if(commandchar == 'i'){
                nodestate = 25;
            }else{
                printf("Error se esperaba i  24 \n");
                return 0;
            }
            break;
        case 25:
            if(commandchar == 't'){
                nodestate = 26;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 26:
            if(commandchar == '='){
                nodestate = 27;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 27:
            if(commandchar == 'k' || commandchar == 'm' || commandchar == 'b'){
                unitchar = commandchar;
                nodestate = 28;
            }else{
                printf("Error se esperaba k, m o b \n");
                return 0;
            }
            break;
        case 28:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 29:
            if(commandchar == 'm'){
                nodestate = 2;
                commandtype = 3;
            }else if(commandchar == 'e'){
                nodestate = 72;
                commandtype = 4;
            }else{
                printf("Error se esperaba m o e \n");
                return 0;
            }
            break;
        case 30:
            if(commandchar == 'y'){
                nodestate = 31;
            }else{
                printf("Error se esperaba y  \n");
                return 0;
            }
            break;
        case 31:
            if(commandchar == 'p'){
                nodestate = 32;
            }else{
                printf("Error se esperaba p \n");
                return 0;
            }
            break;
        case 32:
            if(commandchar == 'e'){
                nodestate = 33;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 33:
            if(commandchar == '='){
                nodestate = 34;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 34:
            if(commandchar == 'p' || commandchar == 'e' || commandchar == 'l'){
                typechar = commandchar;
                nodestate = 35;
            }else{
                printf("Error se esperaba p,e o l \n");
                return 0;
            }
            break;
        case 35:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 36:
            if(commandchar == 'i'){
                nodestate = 37;
            }else{
                printf("Error se esperaba i  36 \n");
                return 0;
            }
            break;
        case 37:
            if(commandchar == 't'){
                nodestate = 38;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 38:
            if(commandchar == '='){
                nodestate = 39;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 39:
            if(commandchar == 'b' || commandchar == 'f' || commandchar == 'w'){
                fitstring[0] = commandchar;
                nodestate = 40;
            }else{
                printf("Error se esperaba b,f o w \n");
                return 0;
            }
            break;
        case 40:
            if(commandchar == 'f'){
                fitstring[1] = 'f';
                nodestate = 41;
            }else{
                printf("Error se esperaba f \n");
                return 0;
            }
            break;
        case 41:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 42:
            if(commandchar == 'e'){
                nodestate = 43;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 43:
            if(commandchar == 'l'){
                nodestate = 44;
            }else{
                printf("Error se esperaba l \n");
                return 0;
            }
            break;
        case 44:
            if(commandchar == 'e'){
                nodestate = 45;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 45:
            if(commandchar == 't'){
                nodestate = 46;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 46:
            if(commandchar == 'e'){
                nodestate = 47;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 47:
            if(commandchar == '='){
                nodestate = 48;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 48:
            if(commandchar == 'f'){
                nodestate = 49;
                deleteform = 1 ;
            }else{
                printf("Error se esperaba f \n");
                return 0;
            }
            break;
        case 49:
            if(commandchar == 'a'){
                nodestate = 50;
                deleteform = 2;
            }else if(commandchar == 'u'){
                nodestate = 53;
                deleteform = 2;
            }else{
                printf("Error se esperaba a o u \n");
                return 0;
            }
            break;
        case 50:
            if(commandchar == 's'){
                nodestate = 51;
                deleteform = 3;
            }else{
                printf("Error se esperaba s \n");
                return 0;
            }
            break;
        case 51:
            if(commandchar == 't'){
                nodestate = 52;
                deleteform = 4;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 52:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 53:
            if(commandchar == 'l'){
                nodestate = 54;
                deleteform = 3;
            }else{
                printf("Error se esperaba l \n");
                return 0;
            }
            break;
        case 54:
            if(commandchar == 'l'){
                nodestate = 52;
                deleteform = 5;
            }else{
                printf("Error se esperaba l \n");
                return 0;
            }
            break;
        case 55:
            if(commandchar == 'a'){
                nodestate = 56;
            }else{
                printf("Error se esperaba a \n");
                return 0;
            }
            break;
        case 56:
            if(commandchar == 'm'){
                nodestate = 57;
            }else{
                printf("Error se esperaba m \n");
                return 0;
            }
            break;
        case 57:
            if(commandchar == 'e'){
                nodestate = 58;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 58:
            if(commandchar == '='){
                nodestate = 59;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 59:
            if(commandchar == '\"'){
                nodestate = 61;
            }else{
                namestring[namecounter] = command[i];
                namecounter += 1;
                nodestate = 60;
            }
            break;
        case 60:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){
            }else{
                namestring[namecounter] = command[i];
                namecounter += 1;
            }
            break;
        case 61:
            if(commandchar == '\"'){
                nodestate = 62;
            }else{
                namestring[namecounter] = command[i];
                namecounter += 1;
            }
            break;
        case 62:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba espacio o salto de linea \n");
                return 0;
            }
            break;
        case 63:
            if(commandchar == 'u'){
                nodestate = 64;
            }else{
                printf("Error se esperaba u \n");
                return 0;
            }
            break;
        case 64:
            if(commandchar == 'n'){
                nodestate = 65;
            }else{
                printf("Error se esperaba n 64\n");
                return 0;
            }
            break;
        case 65:
            if(commandchar == 't'){
                nodestate = 66;
            }else{
                printf("Error se esperaba t \n");
                return 0;
            }
            break;
        case 66:
            if(commandchar == ' '){
                nodestate = 7;
            }else{
                printf("Error se esperaba espacio  \n");
                return 0;
            }
            break;
        case 67:
            if(commandchar == 'n'){
                nodestate = 68;
            }else{
                printf("Error se esperaba n 67 \n");
                return 0;
            }
            break;
        case 68:
            if(commandchar == 'm'){
                nodestate = 69;
            }else{
                printf("Error se esperaba m \n");
                return 0;
            }
            break;
        case 69:
            if(commandchar == 'o'){
                nodestate = 63;
            }else{
                printf("Error se esperaba o \n");
                return 0;
            }
            break;
        case 70:
            if(commandchar == 'd'){
                nodestate = 71;
            }else{
                printf("Error se esperaba d \n");
                return 0;
            }
            break;
        case 71:
            if(commandchar == '='){
                nodestate = 84;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 72:
            if(commandchar == 'p'){
                nodestate = 73;
            }else{
                printf("Error se esperaba p \n");
                return 0;
            }
            break;
        case 73:
            if(commandchar == ' '){
                nodestate = 7;
            }else{
                printf("Error se esperaba espacio \n");
                return 0;
            }
            break;
        case 74:
            if(commandchar == 'x'){
                nodestate = 75;
            }else{
                printf("Error se esperaba x \n");
                return 0;
            }
            break;
        case 75:
            if(commandchar == 'e'){
                nodestate = 76;
            }else{
                printf("Error se esperaba e \n");
                return 0;
            }
            break;
        case 76:
            if(commandchar == 'c'){
                nodestate = 77;
            }else{
                printf("Error se esperaba c \n");
                return 0;
            }
            break;
        case 77:
            if(commandchar == ' '){
                nodestate = 78;
            }else{
                printf("Error se esperaba espacio \n");
                return 0;
            }
            break;
        case 78:
            if(commandchar == ' '){

            }else if(commandchar == '\"'){
                nodestate = 21;
            }else{
                nodestate = 20;
                pathstring[pathcounter] = command[i];
                pathcounter += 1;
            }
            break;
        case 79:
            if(commandchar == 'd'){
                nodestate = 80;
            }else{
                printf("Error se esperaba d \n");
                return 0;
            }
            break;
        case 80:
            if(commandchar == 'd'){
                nodestate = 81;
            }else{
                printf("Error se esperaba d \n");
                return 0;
            }
            break;
        case 81:
            if(commandchar == '='){
                nodestate = 82;
            }else{
                printf("Error se esperaba = \n");
                return 0;
            }
            break;
        case 82:
            if(commandchar == '-'){
                nodestate = 83;
                negative = -1;
            }else if(isdigit(commandchar)){
                nodestate = 83;
                adddisk = (int)commandchar - 48;
            }else{
                printf("Error se esperaba digito o menos \n");
                return 0;
            }
            break;
        case 83:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){
            }else if(isdigit(commandchar)){
                adddisk = (adddisk * 10) + ((int)commandchar - 48);
            }else{
                printf("Error se esperaba espacio, digito o salto de linea \n");
                return 0;
            }
            break;
        case 84:
            if(commandchar == '\"'){
                nodestate = 86;
            }else{
                idstring[idcounter] = command[i];
                idcounter += 1;
                nodestate = 85;
            }
            break;
        case 85:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){
            }else{
                idstring[idcounter] = command[i];
                idcounter += 1;
            }
            break;
        case 86:
            if(commandchar == '\"'){
                nodestate = 87;
            }else{
                idstring[idcounter] = command[i];
                idcounter += 1;
            }
            break;
        case 87:
            if(commandchar == ' '){
                nodestate = 7;
            }else if(commandchar == '\n'){

            }else{
                printf("Error se esperaba salto de linea o espacio \n");
                return 0;
            }
            break;
        }

    }

    adddisk = adddisk * negative;

    if(commentline == 0){
      ejecutioncommand();
    }
}


void ejecutioncommand(){

    int verificationcommand = 1;
    FILE *readfile;


    switch(commandtype){
    case 1: //mkdisk
        if(parametersflags[0] == 0){
            printf("Error no viene el campo obligatorio size \n");
            verificationcommand = 0;
        }else{
            if(sizedisk < 0){
                printf("Error el valor size esta fuera del rango \n");
                verificationcommand = 0;
            }
        }

        if(parametersflags[1] == 0){
            printf("Error no viene el campo obligatorio path \n");
            verificationcommand = 0;
        }

        if(parametersflags[2] == 0){
            unitchar = 'm';
        }else{
            if(!(unitchar == 'k' || unitchar == 'm')){
                printf("Error en el parametro unit \n");
                verificationcommand = 0;
            }
        }

        if(verificationcommand == 1){
            //printf("path: %s, size: %d, unitchar: %c \n",pathstring,sizedisk,unitchar);

            createdisk(pathstring,sizedisk,unitchar);
        }
        initializeparameters();
        break;
    case 2: //mount
        if(parametersflags[1] == 0){
            printf("Error no viene el campo obligatorio path \n");
            verificationcommand = 0;
        }else{
            mounteddisk = fopen(pathstring,"rb");

            if(mounteddisk == NULL)
            {
                printf("Este disco no existe \n");
                mounteddisk = NULL;
                verificationcommand = 0;
            }else{
                fclose(mounteddisk);
            }
        }

        if(parametersflags[6] == 0){
            printf("Error no viene el campo obligatorio name \n");
            verificationcommand = 0;
        }

        if(verificationcommand == 1){
            //printf("path: %s, name: %s \n",pathstring,namestring);
            int i =0;
            int findpath = 0;
            int findname = 0;
            int mount =0 ;

            for(i=0;i<20;i++){
                if(strcmp(ids.lets[i].path,pathstring)==0){
                    findpath = 1;
                    int j=0;
                    for(j=0;j<50;j++){
                        if(strcmp(ids.lets[i].nums[j].name,namestring)==0){
                            findname = 1;
                            printf("Esta particion ya esta montada \n");
                        }
                    }
                }
            }

            if(findpath == 0){
                int i=0;
                for(i=0;i<20;i++){
                    if(ids.mount[i] == 0){
                        ids.mount == 1;
                        strcpy(ids.lets[i].path,pathstring);
                        ids.lets[i].createds = ids.lets[i].createds + 1;
                        int j=0;
                        for(j=0;j<50;j++){
                            if(ids.lets[1].nums[j].mount == 0 && mount==0){
                                strcpy(ids.lets[i].nums[j].name,namestring);
                                ids.lets[i].nums[j].mount = 1;
                                mount = 1;
                                printf("Se monto la particion \n");
                            }
                        }
                    }
                }
            }else if(findname == 0){
                int i=0;
                for(i=0;i<20;i++){
                    if(strcmp(ids.lets[i].path,pathstring)==0){
                        int j=0;
                        for(j=0;j<50;j++){
                            if(ids.lets[i].nums[j].mount == 0 && mount == 0){
                                ids.lets[i].nums[j].mount = 1;
                                strcpy(ids.lets[i].nums[j].name,namestring);
                                mount = 1;
                                printf("Se monto la particion \n");
                            }
                        }
                    }
                }
            }
        }
        initializeparameters();
        break;
    case 3: //rmdisk
        if(parametersflags[1] == 0){
            printf("Error no viene el campo obligatorio path \n");
            verificationcommand = 0;
        }

        if(verificationcommand == 1){
            //printf("path: %s \n",pathstring);
            deletedisk(pathstring);
        }
        initializeparameters();
        break;
    case 4: //rep

        if(parametersflags[1] == 0){
            printf("Error no viene el campo obligatorio path \n");
            verificationcommand = 0;
        }

        if(parametersflags[6] == 0){
            printf("Error no viene el campo obligatorio name \n");
            verificationcommand = 0;
        }else{
            if(!((namestring[0] == 'm' && namestring[1] == 'b' && namestring[2] == 'r') ||
                    (namestring[0] == 'd' && namestring[1] == 'i' && namestring[2] == 's' && namestring[3] == 'k'))){
                printf("Error el campo name no posee los valores disk o mbr %s \n",namestring);
                verificationcommand = 0;
            }
        }

        if(parametersflags[7] == 0){
            printf("Error no viene el campo obligatorio id \n");
            verificationcommand = 0;
        }

        if(verificationcommand == 1){
            //printf("path: %s , name: %s , id: %s \n",pathstring,namestring,idstring);
            int i =0;
            int find = 0;

            for(i=0;i<20;i++){
                if(ids.lets[i].let == idstring[2]){
                    int j=0;
                    for(j=0;j<50;j++){
                        if(ids.lets[i].nums[j].num == idstring[3]){
                            find = 1;
                            //strcpy(diskpath,ids.lets[i].path);
                            if(namestring[0] == 'm'){
                                createtable(ids.lets[i].path,pathstring);
                            }else if(namestring[0] == 'd'){
                                createstruct(ids.lets[i].path,pathstring);
                            }
                        }
                    }
                }
            }
            if(find==0){
                printf("No se encontro el id indicado");
            }
        }
        initializeparameters();
        break;
    case 5: //fidisk
        if(parametersflags[1] == 0){
            printf("Error no viene el campo obligatorio path \n");
            verificationcommand = 0;
        }

        if(parametersflags[6] == 0){
            printf("Error no viene el campo obligatorio name \n");
            verificationcommand = 0;
        }

        if(parametersflags[2] == 0){
            unitchar = 'k';
        }else{
            if(!(unitchar == 'k' || unitchar == 'm' || unitchar == 'b')){
                printf("Error en el parametro unit = %c \n",unitchar);
                verificationcommand = 0;
            }
        }

        if(parametersflags[3] == 0){
            typechar = 'p';
        }else{
            if(!(typechar == 'p' || typechar == 'e' || typechar == 'l')){
                printf("Error en el parametro type \n");
                verificationcommand = 0;
            }
        }

        if(parametersflags[4] == 0){
            fitstring[0] = 'w';
            fitstring[1] = 'f';
        }else{
            if(!(fitstring[0] == 'w' || fitstring[0] == 'b' || fitstring[0] == 'f')){
                printf("Error en el parametro fit \n");
                verificationcommand = 0;
            }
        }

        if(parametersflags[0] == 2){
            if(sizedisk < 0){
                printf("Error el valor size esta fuera del rango \n");
                verificationcommand = 0;
            }
            if(verificationcommand == 1){
                //printf("Crear \n");
                createpartition(pathstring,sizedisk,unitchar,fitstring,typechar,namestring);
            }
        }else if(parametersflags[5] == 2){
            if(!(deleteform == 4 || deleteform == 5)){
                printf("Error el formato delete %d \n",deleteform);
                verificationcommand = 0;
            }
            if(verificationcommand == 1){
                //printf("Eliminar \n");
                deletepartition(pathstring,deleteform,namestring);
            }
        }else if(parametersflags[8] == 2){
            if(verificationcommand == 1){
                //printf("Agregar \n");
                addpartition(adddisk,pathstring,unitchar,namestring);
            }
        }
        initializeparameters();
        break;
    case 6: //unmount
        if(parametersflags[7] == 0){
            printf("Error no viene el campo obligatorio id \n");
            verificationcommand = 0;
        }

        if(verificationcommand == 1){
            printf("id: %s \n",idstring);
        }

        int i =0;
        int find = 0;
        for(i=0;i<20;i++){
            if(ids.lets[i].let == idstring[2]){
                int j=0;
                for(j=0;j<50;j++){
                    if(ids.lets[i].nums[j].num == idstring[3]){
                        find = 1;
                        ids.lets[i].nums[j].mount = 0;
                        strcpy(ids.lets[i].nums[j].name,"");
                        printf("Se desmonto la particion \n");
                    }
                }
            }
        }

        if(find == 0){
            printf("No se encontro este id\n");
        }

        initializeparameters();
        break;
    case 7: //exec

        readfile = fopen(pathstring,"r");

        if(readfile != NULL){

            while (feof(readfile) == 0)
            {
                char commandconsole[100];
                fgets(commandconsole,100,readfile);
                printf("%s",commandconsole);
                parserCommand(commandconsole);
            }
            fclose(readfile);

        }

        break;
    }

}


void initializeparameters(){
    sizedisk = 0;
    int i=0;
    for(i=0;i<40;i++){
        pathstring[i] = 0;
        namestring[i] = 0;
        idstring[i] = 0;
    }
    unitchar = 'b';
    typechar = 'p';
    fitstring[0] = 'w';
    fitstring[1] ='f';
    deleteform = 0;
    adddisk = 0;
    for(i=0;i<9;i++){
        parametersflags[i] = 0;
    }
    firstparameter = 1;
    commandtype = 0;
    initnodestate = 0;
}
