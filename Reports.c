#include "Reports.h"


createstruct(char *pathstring,char *imagepath){
    MBR mbr;
    FILE *tempfile;
    int k=0;

    disk = fopen(pathstring,"rb+");

    tempfile = fopen("/home/henry/Reportes/grafico.txt","w");
    fprintf(tempfile,"%s","digraph G{\n node[shape =record];\nstruct1 [shape= record,label =\" <f0> MBR ");

    if(disk !=NULL && tempfile !=NULL){
        rewind(disk);
        fread(&mbr,sizeof(MBR),1,disk);

        int iter = 1;
        int iterlog = 1;
        char d[10];
        char dlog[10];
        int firstlog = 0;

        for(k=0;k<4;k++){
            if(mbr.partitions[k].statuspartition == 'a'){
                if(mbr.partitions[k].typepartition == 'e'){
                    fprintf(tempfile,"%s","|{<f");
                    sprintf(d,"%d",iter);
                    iter = iter + 1;
                    fprintf(tempfile,d);
                    fprintf(tempfile,"%s","> ");
                    fprintf(tempfile,mbr.partitions[k].namepartition);
                    fprintf(tempfile,"%s","|{");

                    EBR ebr;
                    int initpart = mbr.partitions[k].initbytepartition;

                    do{
                        fseek(disk,initpart,SEEK_SET);
                        fread(&ebr,sizeof(EBR),1,disk);

                        if(firstlog == 0){
                            fprintf(tempfile,"%s","<");
                            firstlog = 1;
                        }else{
                            fprintf(tempfile,"%s","|<");
                        }
                        sprintf(dlog,"%d",iterlog);
                        iterlog = iterlog + 1;
                        fprintf(tempfile,d);
                        fprintf(tempfile,dlog);
                        fprintf(tempfile,"%s","> EBR");
                        if(ebr.status == 'a'){
                            fprintf(tempfile,"%s","|");
                            fprintf(tempfile,"%s",ebr.name);
                        }
                        if((ebr.next - (ebr.start + ebr.size)) > 0){
                            fprintf(tempfile,"%s","|Libre");
                        }
                        initpart = ebr.next;
                    }while(ebr.next > -1);
                    fprintf(tempfile,"%s","}}");
                }else{
                    fprintf(tempfile,"%s","|<f");
                    sprintf(d,"%d",iter);
                    iter = iter + 1;
                    fprintf(tempfile,d);
                    fprintf(tempfile,"%s","> ");
                    fprintf(tempfile,mbr.partitions[k].namepartition);
                }
            }else{
                fprintf(tempfile,"%s","|Libre");
            }
        }

        fprintf(tempfile,"%s","\" ];\n}");
        fclose(tempfile);
        printf("Se creo el grafico\n");

        fclose(disk);
    }else{
        printf("Error al abrir los archivos \n");
    }


    /*static char prefix[] = "dot -Tpng /home/grafico.txt -o ";
    char *cmd = malloc (sizeof (prefix) + strlen (imagepath));

    strcpy(cmd,prefix);
    strcpy(cmd,imagepath);*/
    system("dot -Tpng /home/henry/Reportes/grafico.txt -o /home/henry/Reportes/grafico.png");

    //system(cmd);
    //free(cmd);
}


createtable(char *pathstring,char *imagepath){
    MBR mbr;
    FILE *tempfile;
    FILE * imagefile;
    int k=0;



    disk = fopen(pathstring,"rb+");

    tempfile = fopen("/home/henry/Reportes/tablas.txt","w");
    fprintf(tempfile,"%s","digraph G{\n rankdir=LR;\n node[shape =record];\nstruct1 [shape= record,label =\"{Nombre|{Valor}}");

    if(disk !=NULL && tempfile !=NULL){
        rewind(disk);
        fread(&mbr,sizeof(MBR),1,disk);

        int initpart = -1;
        int find = 1;

        fprintf(tempfile,"%s","|{mbr_tamano|{");
        fprintf(tempfile,"%d",mbr.sizedisk);
        fprintf(tempfile,"%s","}}");
        fprintf(tempfile,"%s","|{mbr_fecha_creacion|{");
        fprintf(tempfile,"%d",mbr.datetimecreation);
        fprintf(tempfile,"%s","}}");
        fprintf(tempfile,"%s","|{mbr_signature|{");
        fprintf(tempfile,"%d",mbr.signature);
        fprintf(tempfile,"%s","}}");

        for(k=0;k<4;k++){
            if(mbr.partitions[k].statuspartition == 'a'){
                fprintf(tempfile,"%s","|{part_status_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%c",mbr.partitions[k].statuspartition);
                fprintf(tempfile,"%s","}}");
                fprintf(tempfile,"%s","|{part_type_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%c",mbr.partitions[k].typepartition);
                fprintf(tempfile,"%s","}}");
                fprintf(tempfile,"%s","|{part_fit_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%c",mbr.partitions[k].fitpartition);
                fprintf(tempfile,"%s","}}");
                fprintf(tempfile,"%s","|{part_start_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%d",mbr.partitions[k].initbytepartition);
                fprintf(tempfile,"%s","}}");
                fprintf(tempfile,"%s","|{part_size_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%d",mbr.partitions[k].sizepartition);
                fprintf(tempfile,"%s","}}");
                fprintf(tempfile,"%s","|{part_name_");
                fprintf(tempfile,"%d",find);
                fprintf(tempfile,"%s","|{");
                fprintf(tempfile,"%s",mbr.partitions[k].namepartition);
                fprintf(tempfile,"%s","}}");
                find += 1;

                if(mbr.partitions[k].typepartition == 'e'){
                    initpart = mbr.partitions[k].initbytepartition;
                }
            }
        }

        fprintf(tempfile,"%s","\" ];\n");

        EBR ebr;
        int countlogpartition = 0;

        if(initpart > 0){
            do{
                fseek(disk,initpart,SEEK_SET);
                fread(&ebr,sizeof(EBR),1,disk);

                if(ebr.status == 'a'){
                    fprintf(tempfile,"%s","\nstructa");
                    fprintf(tempfile,"%d",countlogpartition);
                    countlogpartition +=1;
                    fprintf(tempfile,"%s"," [shape= record,label =\"{Nombre|{Valor}}");
                    fprintf(tempfile,"%s","|{part_status_1|{");
                    fprintf(tempfile,"%c",ebr.status);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","|{part_fit_1|{");
                    fprintf(tempfile,"%c",ebr.fit);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","|{part_start_1|{");
                    fprintf(tempfile,"%d",ebr.start);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","|{part_size_1|{");
                    fprintf(tempfile,"%d",ebr.size);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","|{part_next_1|{");
                    fprintf(tempfile,"%d",ebr.next);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","|{part_name_1|{");
                    fprintf(tempfile,"%s",ebr.name);
                    fprintf(tempfile,"%s","}}");
                    fprintf(tempfile,"%s","\" ];\n");
                }
                initpart = ebr.next;
            }while(ebr.next > -1);
        }


        fprintf(tempfile,"%s","}");
        fclose(tempfile);
        printf("Se creo la tabla \n");

        fclose(disk);
    }else{
        printf("Error al abrir los archivos \n");
    }

    //static char pref[] = "dot -Tpng /home/henry/Reportes/tablas.txt -o /home/henry/Reportes/tablas.png";
    //char *cmde = malloc (sizeof(pref) + sizeof(imagepath));

    //strcpy(cmde,pref);
    //strcpy(cmde,imagepath);
    //printf("file %s",cmde);

    system("dot -Tpng /home/henry/Reportes/tablas.txt -o /home/henry/Reportes/tablas.png");
    //free(cmde);
}
