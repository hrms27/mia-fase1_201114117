TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    Disk.c \
    Partitions.c \
    Reports.c

HEADERS += \
    StructsDisk.h \
    Disk.h \
    Partitions.h \
    Reports.h

