#include <Disk.h>

void createdisk(char *pathstring,int sizedisk,char unitchar)
{

    char control = 0;
    int sizecontrol = 0;

    newdisk = fopen(pathstring,"rb");

    if(newdisk != NULL)
    {
        printf("Ya existe este disco\n");
        fclose(newdisk);
        control = 1;
        newdisk = NULL;
    }else{
        static char prefix[] = "mkdir -p ";
        char *cmd = malloc (sizeof (prefix) + strlen (pathstring));

        strcpy(cmd,prefix);
        strcpy(cmd,pathstring);
        system(cmd);
        free(cmd);

        newdisk = fopen(pathstring,"rb");
    }


    if(control==0)
    {
    MBR mbr;

    newdisk = fopen(pathstring,"ab");

    if(unitchar == 'k'){
        sizecontrol = 1024;
    }else if(unitchar == 'm'){
        sizecontrol = 1048576;
    }

    //strftime(time,128,"%d-%m-%y %H:%M:%S",time(NULL));

    mbr.sizedisk = sizedisk * sizecontrol;
    mbr.datetimecreation = time(NULL);
    mbr.signature = rand () % 11 + 1; ;

    int k = 0;
    for(k = 0; k < 4; k++)
    {
        mbr.partitions[k].statuspartition = 'n'; // a activa n no activa
        mbr.partitions[k].typepartition = '0';
        mbr.partitions[k].fitpartition = 'w';
        mbr.partitions[k].initbytepartition = 0;
        mbr.partitions[k].sizepartition = 0;
        strcpy(mbr.partitions[k].namepartition,"");
    }

    rewind(newdisk);


    fwrite(&mbr,sizeof(MBR),1,newdisk);



    int contador = (sizedisk * sizecontrol) - sizeof(MBR);

    char c = '*';
    for(k = 0; k < contador; k++){
        fwrite(&c,sizeof(char),1,newdisk);
    }

    fclose(newdisk);
    newdisk = NULL;

    printf("Disco creado correctamente\n");

    }
}

void deletedisk(char *pathstring){
    FILE *deletedisk;

    deletedisk = fopen(pathstring, "r" );

      if(deletedisk != NULL)
      {
         fclose(deletedisk);
         if(remove(pathstring) == 0){
             printf("Disco eliminado exitosamente \n");
         }else{
             printf("No se pudo eliminar el disco \n");
         }
         fclose(deletedisk);
      }
      else{
          printf("El disco no existe \n" );
      }
}
