#ifndef STRUCTSDISK_H
#define STRUCTSDISK_H

#include <stdio.h>
#include <time.h>

struct Partition
{
    char statuspartition; //estatus de la particion
    char typepartition; //tipo de particion
    char fitpartition;
    unsigned int initbytepartition; //inicio de la particion
    unsigned int sizepartition; //tamano de la particion
    char namepartition[16];//nombre de la particion
};
typedef struct Partition Partition;

struct MBR
{
    int sizedisk;
    time_t datetimecreation;
    int signature;
    Partition partitions[4];
};
typedef struct MBR MBR;

struct EBR
{
    char status;
    char fit;
    int start;
    int size;
    int next;
    char name[16];
};
typedef struct EBR EBR;

struct NUM
{
    int mount;
    char name[16];
    int num;
};
typedef struct NUM NUM;

struct LET
{
    char let;
    char path[100];
    int createds;
    NUM nums[50];
};
typedef struct LET LET;

struct IDS
{
    LET lets[20];
    int mount[20];
};
typedef struct IDS IDS;
#endif
