#ifndef PARTITIONS_H
#define PARTITIONS_H

#include "StructsDisk.h"

FILE * disk;


void createpartition(char *pathstring,int sizedisk,char unitchar,char *fitstring,char typechar,char *namestring);

void deletepartition(char *pathstring,int deleteformp,char *namestring);

void addpartition(int addsize,char *pathstring, char unitchar, char *namestring);

#endif
